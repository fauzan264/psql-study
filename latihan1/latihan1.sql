-- Time is your practice
-- 1. Buatlah query untuk menampilkan seluruh data karyawan dari table employees yang diurutkan berdasarkan email paling terakhir.
-- 2. Buatlah query untuk menampilkan data karyawan yang gajinya lebih besar 3200.00 sampai dengan 12000.00.
-- 3. Buatlah query untuk menampilkan data karyawan yang memiliki huruf A diawal nama depannya.
-- 4. Buatlah query untuk menampilkan data karyawan yang memiliki kode karyawan diantaranya 103, 115, 196, 187, 102 dan 100
-- 5. Buatlah query untuk menampilkan data karyawan yang nama belakangnya memiliki huruf kedua u.
-- 6. Buatlah query untuk menampilkan kode department apa saja yang ada di tabel employees secara unique
-- 7. Buatlah query untuk menampilkan nama lengkap karyawan, kode jabatan, gaji setahun dari table employees yang kode manager sama dengan 100.
-- 8. Buatlah query untuk menampilkan nama belakang, gaji perbulan, kode jabatan dari table employees yang tidak memiliki komisi
-- 9. Buatlah query untuk menampilkan data karyawan yang bukan dari jabatan IT_PROG dan SH_CLERK.

-- nomor 1
SELECT 
    first_name as nama_depan, 
    last_name as nama_belakang, 
    email 
FROM 
    employees 
ORDER BY 
    employee_id DESC;


-- nomor 2
SELECT 
    first_name as nama_depan, 
    last_name as nama_belakang, 
    salary as gaji 
FROM 
    employees 
WHERE 
    salary BETWEEN 3200.00 AND 12000.00 
ORDER BY salary ASC;


-- nomor 3
SELECT 
    first_name as nama_depan, 
    last_name as nama_belakang, 
    salary as gaji 
FROM 
    employees 
WHERE 
    first_name LIKE 'A%';

-- nomor 4
SELECT  
    employee_id as id_karyawan, 
    first_name as nama_depan, 
    last_name as nama_belakang, 
    salary as gaji 
FROM 
    employees 
WHERE 
    employee_id IN (103,115,196,187,102, 100) 
ORDER BY 
    employee_id ASC;

-- nomor 5
SELECT 
    employee_id as id_karyawan, 
    first_name as nama_depan, 
    last_name as nama_belakang, 
    salary as gaji 
FROM 
    employees 
WHERE 
    first_name LIKE '_u%' ORDER BY first_name;


-- nomor 6
SELECT 
    DISTINCT department_id as id_departement 
FROM 
    employees 
ORDER BY 
    department_id;


-- nomor 7
SELECT 
    first_name || ' ' || last_name as nama_lengkap, 
    job_id as kode_jabatan, 
    salary as gaji_bulanan, 
    salary * 12 as gaji_setahun 
FROM 
    employees 
WHERE 
    manager_id = 100 
ORDER BY 
    nama_lengkap;


-- nomor 8
SELECT 
    last_name as nama_depan, 
    salary as gaji, 
    job_id
    -- commission_pct as komisi_persen 
FROM 
    employees 
WHERE 
    commission_pct is null;


-- nomor 9
SELECT 
    first_name as nama_depan, 
    last_name as nama_belakang, 
    job_id 
FROM 
    employees 
WHERE 
    job_id NOT IN ('IT_PROG', 'SH_CLERK');