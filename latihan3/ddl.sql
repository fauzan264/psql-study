-- create, digunakan untuk membuat object pada database seperti table, view, sequence, schema, dll.
-- drop, digunakan untuk menghapus object pada database seperti table, view, sequence, schema, dll.
-- alter, digunakan untuk memodifikasi object pada database seperti menambahkan kolom pada table, menambahkan constraint pada kolom tertentu.
-- truncate, digunakan untuk menghapus semua record dalam suatu table.
-- rename, digunakan untuk mengubah nama database yang ada.

-- grant, untuk memberikan ijin akses terhadap object di database.
-- revoke, untuk mencabut ijin akses terhadap object di dalam user tsb.