begin;

insert into regions(region_id, region_name) values(6, 'Oceania');

-- seperti melakukan checkpoint
-- savepoint insert_1;

delete from regions where region_id in (5,6);

-- kembali ke checkpoint insert_1
rollback to savepoint insert_1;

-- melakukan commit 
release insert_1;