-- tambah data
insert into regions(region_id, region_name) values (5, 'Asia Tenggara');

-- ubah data
update regions set region_name = 'Oceania';

-- hapus data
delete from regions where region_id=5;