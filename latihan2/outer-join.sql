-- Outer Join
-- Right Join
-- Berfungsi untuk memanggil semua data yang di join ke kanan
-- data yang tidak ada relasi akan bernilai null
SELECT
    d.department_id as kode_department,
    d.department_name as nama_department,
    l.location_id as kode_lokasi,
    l.street_address as alamat
FROM locations l RIGHT JOIN departments d
ON (l.location_id = d.location_id);


-- Left Join
-- kebalikan dari RIGHT JOIN
SELECT
    d.department_id as kode_department,
    d.department_name as nama_department,
    l.location_id as kode_lokasi,
    l.street_address as alamat
FROM locations l LEFT JOIN departments d
ON (l.location_id = d.location_id);

-- Inner Join
-- memanggil yang hanya memiliki relasi saja
SELECT
    d.department_id as kode_department,
    d.department_name as nama_department,
    e.employee_id as kode_karyawan,
    concat(e.first_name, ' ', e.last_name) as nama_karyawan
FROM 
    employees e INNER JOIN departments d
ON
    (e.department_id = d.department_id);