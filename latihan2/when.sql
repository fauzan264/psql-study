-- Seleksi case dengan When di PostgreSQL

-- CASE WHEN condition THEN result
--      [WHEN ...]
--      [ELSE result]
-- END


SELECT
    employee_id as kode_karyawan,
    commission_pct as besar_komisi,
    CASE
        WHEN COALESCE(commission_pct, 0) = 0
        THEN
            'Tidak memiliki komisi'
        ELSE 
            CONCAT('Memiliki komisi sebesar ', commission_pct)
    END as deskripsi
FROM employees;
        