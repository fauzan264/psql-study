-- INNER JOIN
-- mirip dengan NATURAL JOIN
-- pervedaannya lebih bebas dalam menentukan yang di join
-- dengan menghubungkan menggunakan where atau on

-- contoh menggunakan ON
SELECT
    d.department_id as kode,
    d.department_name as nama_divisi,
    d.manager_id as kode_manager,
    concat(e.first_name, ' ', e.last_name) as nama_manager
FROM
    departments d
JOIN
    employees e
ON
    (d.manager_id = e.employee_id);


-- contoh menggunakan WHERE
SELECT
    d.department_id as kode,
    d.department_name as nama_divisi,
    d.manager_id as kode_manager,
    concat(e.first_name, ' ', e.last_name) as nama_manager
FROM
    departments d, employees e
WHERE
    d.manager_id = e.employee_id;