-- SELFT JOIN
-- melakukan relasi ke tabel dirinya sendiri

SELECT
    e.employee_id as kode_karyawan,
    concat(e.first_name, ' ', e.last_name) as nama_karyawan,
    m.employee_id as kode_manager,
    concat(m.first_name, ' ', m.last_name) as nama_manager
FROM
    employees e LEFT JOIN employees m ON (e.manager_id = m.employee_id);