-- menggunakan where
-- contoh penggunaan where
SELECT
    manager_id, count(*) as jumlah_karyawan
FROM
    employees
WHERE
    salary >= 5000
GROUP BY
    manager_id
ORDER BY 
    manager_id;



-- contoh penggunaan having
SELECT
    manager_id, count(*) as jumlah_karyawan
FROM
    employees
GROUP BY
    manager_id
HAVING
    count(*) >= 5
ORDER BY manager_id;


-- contoh penggunaan where dan having
SELECT
    manager_id, count(*) as jumlah_karyawan
FROM
    employees
WHERE
    salary >= 5000
GROUP BY
    manager_id
HAVING
    count(*) >= 5
ORDER BY
    manager_id;