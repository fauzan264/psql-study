-- NATURAL JOIN
-- syarat
-- tabel yang berelasi, column name harus sama
-- harus memiliki constrain foreign key

SELECT
    l.location_id as kode_lokasi,
    l.city as kota,
    l.state_province as provinsi,
    c.country_name as negara
FROM
    locations l natural join countries c;