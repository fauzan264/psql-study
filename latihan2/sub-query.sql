-- SUB QUERY

-- penempatan pada select
SELECT
    e.employee_id as nik,
    e.first_name as nama,
    (select j.job_title from jobs j where j.job_id = e.job_id) as jabatan,
    (select j2.min_salary from jobs j2 where j2.job_id = e.job_id) as minimum_salary
FROM
    employees e
LIMIT 10;


-- penempatan pada where
SELECT 
    e.employee_id as nik,
    e.first_name as nama,
    e.salary as gaji_sebulan
FROM
    employees e
WHERE
    e.salary > (select avg(j.max_salary) from jobs j)
LIMIT 10;


-- menggunakan operator = Any
SELECT 
    job_id as id,
    round(
        avg(j.max_salary)
    ) as gaji_rata
FROM
    jobs j 
GROUP BY j.job_id;

SELECT
    e.employee_id as nik,
    e.first_name as nama,
    e.salary as gaji_sebulan
FROM 
    employees e 
WHERE
    e.salary = any (
        SELECT 
            round(avg(j.max_salary)) as gaji_rata
        FROM jobs j
        GROUP BY j.job_id
    );


-- menggunakan operator IN
-- penempatan menggunakan operator  > Any
SELECT 
    round(avg(j.max_salary)) as gaji_rata
FROM jobs j 
GROUP BY j.job_id
HAVING avg(j.max_salary) < 20000;

SELECT
    e.employee_id as nik,
    e.first_name as nama,
    e.salary as gaji_sebulan
FROM
    employees e 
WHERE
    e.salary > any (
        SELECT
            round(avg(j.max_salary)) as gaji_rata
        FROM jobs j
        GROUP BY j.job_id
        HAVING avg(j.max_salary) < 20000
    );