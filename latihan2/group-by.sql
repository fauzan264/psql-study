
-- Group by kode

SELECT
    department_id as kode,
    count(*) as jumlah_karyawan,
    (sum(salary) * 12) as total_salary
FROM
    employees
GROUP BY
    kode
ORDER BY
    kode;
    


-- Group by divisi dan manager
SELECT
    department_id as divisi,
    manager_id as manager,
    count(*) as jumlah_karyawan,
    (sum(salary) * 12) as total_salary
FROM
    employees
GROUP BY divisi, manager
ORDER BY divisi;
