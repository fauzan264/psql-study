SELECT
    ROUND(avg(salary), 2) as rata_gaji,
    COUNT(*) as jml_baris,
    MAX(salary) as max_gaji,
    MIN(salary) as min_gaji,
    SUM(salary) as total_salary
FROM
    employees;