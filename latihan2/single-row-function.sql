
-- format string
SELECT
    employee_id as kode,
    upper(first_name) as nama_depan_kapital,
    last_name as nama_belakang,
    length(last_name) as jumlah,
    concat(first_name, ' ', last_name) as nama_lengkap
FROM 
    employees 
LIMIT 10;


-- format number
SELECT
    COALESCE(commission_pct, 0) as commission_pct,
    salary as gaji,
    mod(2,4) as sisa_bagi,
    power((salary / 1000), 2) as pangkat,
    sqrt(50) as akar
FROM
    employees
LIMIT 10;